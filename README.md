## Integrantes:
:star2: Miryam Godoy.
:star2: Maria Lucia Salinas.
## Intrucciones
- En el directorio /rest-api-nodejs se encuentra el backend, luego debe seguir los sgtes pasos. 
- Crear una base de datos llamado backendParcial2 en el Postgres.
- En el directorio /src/database/ encontrara un archivo database.js en el cual debe agregar el username de la base de datos Postgres y su password correspondiente.
- En el directorio /sql se encuentra un archivo db.sql el cual le permitira crear tablas e insertar datos en la base de datos.
- Para instalar las librerias y dependencias del proyecto res-api-nodejs ejecute el comando npm install.
- Para ejecutar el projecto res-api-nodejs en modo desarrollador ejecute el sgte comando npm run dev.
- Para ejecutar el projecto res-api-nodejs en modo produccion ejecute primeramente npm run build y luego npm start.
- En el directorio /front-resp/reservaApp se encuentra el frontend del backend.
- Para instalar librerias en el frontend debe ejecutar el comando npm install en su terminal.
- Para ejecutar el frontend ejecute el comando npm start. 