import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapaComponent } from './pages/mapa/mapa.component';
import { ReservaComponent } from './pages/reserva/reserva.component';
import { ReservasComponent } from './pages/reservas/reservas.component';

const routes: Routes = [
  {path:'reserva', component: ReservaComponent},
  {path: 'reservas', component: ReservasComponent},
  {path: 'mapa', component: MapaComponent},
  {path: '**', pathMatch:'full', redirectTo: 'reservas'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
