import { RestauranteModel } from "./restaurante.model";


export class MesaModel{
    id: number;
    nombre_mesa: string;
    restaurante: RestauranteModel;
    posicionX: number;
    posicionY: number;
    nroPiso: number;
    capComensales: number;
}