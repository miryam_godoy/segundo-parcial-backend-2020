import { ClienteModel } from './cliente.model';
import { MesaModel } from './mesa.model';
import { RestauranteModel } from './restaurante.model';



export class ReservaModel{
    id: number;
    restaurante: RestauranteModel;
    mesa: MesaModel;
    fecha: string;
    horaInicio: string;
    horaFin: string;
    cliente: ClienteModel;
    cantidad: number;
}