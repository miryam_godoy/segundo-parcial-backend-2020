import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { FormsModule } from '@angular/forms';

import Swal from 'sweetalert2';




import { ReservaModel } from 'src/app/models/reserva.model';
import { RestauranteModel } from 'src/app/models/restaurante.model';
import { MesaModel } from 'src/app/models/mesa.model';
import { ClienteModel } from 'src/app/models/cliente.model';




@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {

  //reservas: ReservaModel[] = [];
  restaurantes: RestauranteModel[] = [];
  hora: string[] = ["12 a 13", "13 a 14", "14 a 15", "15 a 16","16 a 17" ,"17 a 18", "18 a 19", "19 a 20",
                    "20 a 21", "21 a 22"];
  cargando = true;
  page = 1;

  clientes: ClienteModel[] = [];
  mesas: MesaModel[] = [];
  rest: any;
  cli: any;
  horaInicio : any;
  horaFin: any;
  fecha: Date;

  nombre: any;
  apellido: any;

  clienteNombre: any;

  cedula: any;

  cantidad: any;

  constructor( private reservaServicio: ServicioService) { }

  ngOnInit(): void {

    this.reservaServicio.getRestaurantes()
    .subscribe(resp=>{
     this.restaurantes = resp;
    console.log(this.restaurantes);

    });
  }



  buscarMesas(){
    
      this.cargando = false;

    this.reservaServicio.getMesas(this.rest.id, this.fecha, this.horaInicio, this.horaFin)
    .subscribe(resp =>{
      console.log(resp);
      this.mesas = resp;
    });
    
    

    
  

  }

  getClientes(){
    this.reservaServicio.getClientes()
    .subscribe(resp =>{
      this.clientes = resp;
    })
  }

  idCliente: any;

  buscarClientePorCedula(){
    this.reservaServicio.getOneClienteByCedula(this.cedula)
    .subscribe(resp =>{
      console.log(resp);
      if( resp === null){
        this.clienteNombre = "No existe cliente";

      }else{

        this.clienteNombre = resp.nombre + " " + resp.apellido;
        this.cli = resp;


      }
    });

  }

  registrarCliente(){

    this.reservaServicio.setCliente(this.nombre, this.apellido, this.cedula)
    .subscribe(resp =>{
      console.log(resp);
      this.cli = resp.data;
      console.log(this.cli.id);
      this.cedula = undefined;

    });

    Swal.fire({
      title: `El cliente ${this.nombre} ${this.apellido}` ,
      text: 'Se guardo correctamente',
       icon: 'success',
       confirmButtonText: 'Ok'
    });


  }

  crearReserva(i){
  

   // console.log(this.cli.id);

    if(this.rest.id === undefined || this.fecha === undefined || this.horaInicio === undefined
      || this.horaFin === undefined || this.cli === undefined || this.cantidad === undefined){
        Swal.fire({
          title: `Ocurrio un error al intentar reservar` ,
          text: 'Revise que los campos esten correctos',
           icon: 'error',
           confirmButtonText: 'Ok'
        });
      }else{
        this.reservaServicio.setReserva(this.rest.id, this.fecha, this.horaInicio, this.horaFin,
          this.mesas[i].id, this.cli.id, this.cantidad)
          .subscribe(resp =>{
            console.log(resp);
          });
           this.mesas.splice(i,1);
          Swal.fire({
            title: `El reserva del cliente ${this.nombre} ${this.apellido}` ,
            text: 'Se guardo correctamente',
             icon: 'success',
             confirmButtonText: 'Ok'
          });
      }
    
    
  }

}
