import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';

import Swal from 'sweetalert2';


import { ReservaModel } from 'src/app/models/reserva.model';
import { RestauranteModel } from 'src/app/models/restaurante.model';

@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.component.html',
  styleUrls: ['./reservas.component.css']
})
export class ReservasComponent implements OnInit {

  reservas: ReservaModel[] = [];
  restaurantes: RestauranteModel[] = [];
  rest: any;
  fecha: Date;

  cedula: any;

  cargando = true;
  page = 1;

  constructor(private reservaServicio: ServicioService) { }

  ngOnInit(): void {
    this.reservaServicio.getReservas()
    .subscribe(resp =>{
      console.log(resp);
      this.reservas = resp;
      this.cargando = false;
    });

    this.reservaServicio.getRestaurantes()
    .subscribe(resp=>{
     this.restaurantes = resp;
    console.log(this.restaurantes);

    });  
  }

  buscar(){
    if(this.rest != undefined){
      this.reservaServicio.getReservaByRestaurante(this.rest.id)
      .subscribe(resp =>{
        console.log(resp);
        this.reservas = resp;
      });

    }else if(this.fecha != undefined){
      this.reservaServicio.getReservaByFecha(this.fecha)
      .subscribe(resp=>{
        console.log(resp);
        this.reservas = resp;
      })
      
    }else if( this.cedula != undefined){
      /*this.reservaServicio.getReservaByCliente(this.cedula)
      .subscribe(resp=>{
        console.log(resp);
        this.reservas = resp;
      })*/
     const  tempCliente : ReservaModel[] = [];
      for(let i = 0; i< this.reservas.length; i++){
        if(this.reservas[i].cliente.cedula === this.cedula){
          tempCliente.push(this.reservas[i]);
        }
      }
      this.reservas = tempCliente;

    }else{

      Swal.fire({
        title: `Ocurrio un error al buscar reservas` ,
        text: 'La busqueda solo se puede hacer por restaurante, por fecha o por cedula del cliente',
         icon: 'error',
         confirmButtonText: 'Ok'
      });

    }
  }

  restaurar(){
    this.rest = undefined;
    this.fecha = undefined;
    this.cedula = undefined;
    this.ngOnInit();
  }

}
