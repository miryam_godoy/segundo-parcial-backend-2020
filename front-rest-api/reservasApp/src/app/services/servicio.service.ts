import { Injectable } from '@angular/core';
import { HttpClient,  HttpParams, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ReservaModel } from '../models/reserva.model';
import { RestauranteModel } from '../models/restaurante.model';
import { MesaModel } from '../models/mesa.model';
import { ClienteModel } from '../models/cliente.model';







@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  //private urlTp = 'http://localhost:4000/api';

  constructor(private http: HttpClient) { }

  getReservas(){
    return this.http.get(`/api/reservas`)
    .pipe(
      map(this.crearArregloReservas),

    );
  }


  private crearArregloReservas(reservasObj) {

    const reservas: ReservaModel[] = [];

    console.log('lo que trae del puto array');
    console.log(reservasObj);


    if (reservasObj === null) { return []; }
    if (reservasObj.data.reservas === null) { return []; }

    reservasObj.data.forEach((item) => {

      const reserva = new ReservaModel();
      reserva.id = item.id;
      reserva.restaurante = item.Restaurante;
      reserva.mesa = item.Mesa;
      reserva.fecha = item.fecha;
      reserva.horaInicio = item.hora_inicio;
      reserva.horaFin = item.hora_fin;
      reserva.cliente = item.Cliente;
      reserva.cantidad = item.cantidad;
      reservas.push(reserva);
    });

    return reservas;

  }

  getRestaurantes(){
    return this.http.get(`/api/restaurantes`)
    .pipe(
      map(this.crearArregloRestaurantes),

    );
  }

  private crearArregloRestaurantes(restaurantesObj) {

    const restaurantes: RestauranteModel[] = [];

    //console.log('lo que trae del puto array');
    //console.log(reservasObj);


    if (restaurantesObj === null) { return []; }
    if (restaurantesObj.data.reservas === null) { return []; }

    restaurantesObj.data.forEach((item) => {

      const restaurante = new RestauranteModel();
      restaurante.id = item.id;
      restaurante.nombre = item.nombre;
      restaurante.direccion = item.direccion;
      restaurantes.push(restaurante);
    });

    return restaurantes;

  }

  getMesas(restaurante, fecha, hora_inicio, hora_fin){


    /*let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json');
   let httpParams = new HttpParams()
                        .set('restaurante', restaurante)
                    .set('fecha', fecha)
                    .set('hora_inicio', hora_inicio)
                    .set('hora_fin', hora_fin);*/

    /*let params = new HttpParams();
    params = params.append('restaurante', `${restaurante}`);
    params = params.append('fecha', `${fecha}`);
    params = params.append('hora_inicio', `${hora_inicio}`);
    params = params.append('hora_fin', `${hora_fin}`);
*/


    const body= {
        "restaurante": restaurante,
        "fecha": fecha,
        "hora_inicio": hora_inicio,
        "hora_fin": hora_fin
    };
    return this.http.post(`/api/mesas/mesasDisponibles`, body)
    .pipe(
      map(this.crearArregloMesas),

    );
  }

  private crearArregloMesas(mesasObj) {

    const mesas: MesaModel[] = [];

    //console.log('lo que trae del puto array');
    //console.log(reservasObj);


    if (mesasObj === null) { return []; }
    if (mesasObj.data.mesas === null) { return []; }

    mesasObj.data.forEach((item) => {

      const mesa = new MesaModel();
      mesa.id = item.id;
      mesa.nombre_mesa = item.nombre_mesa;
      mesa.restaurante = item.resturanteid;
      mesa.posicionX = item.posicion_x;
      mesa.posicionY = item.posicion_y;
      mesa.nroPiso = item.nro_piso;
      mesa.capComensales = item.cap_comensales;
      mesas.push(mesa);
    });

    return mesas;

  }

  getClientes(){
    return this.http.get(`/api/clientes`)
    .pipe(
      map(this.crearArregloClientes),

    );
  }

  private crearArregloClientes(clientesObj) {

    const clientes: ClienteModel[] = [];

    //console.log('lo que trae del puto array');
    //console.log(reservasObj);


    if (clientesObj === null) { return []; }
    if (clientesObj.data.mesas === null) { return []; }

    clientesObj.data.forEach((item) => {

      const cliente = new ClienteModel();
      cliente.id = item.id;
      cliente.nombre = item.nombre;
      cliente.apellido = item.apellido;
      clientes.push(cliente);
    });

    return clientes;

  }

  getOneClienteByCedula(cedula){
    return this.http.get<ClienteModel>(`/api/clientes/${cedula}`);

  }

  setCliente(nombre, apellido, cedula){
    const body = {
      "nombre": nombre,
      "apellido": apellido,
      "cedula": cedula,
      
    };

    return this.http.post(`/api/clientes`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );

    

    
  
  }

  setReserva(restaurante, fecha, hora_inicio, hora_fin, mesa, cliente, cantidad){
    const body = {
      "restaurante": restaurante,
      "mesa": mesa,
      "fecha": fecha,
    "hora_inicio": hora_inicio,
     "hora_fin" : hora_fin,
    "cliente": cliente,
    "cantidad": cantidad
      
    };

    return this.http.post(`/api/reservas/addReserva`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );

    

    
  
  }
  

  getReservaByRestaurante(restauranteId){
    let params = new HttpParams();
    params = params.append('restaurante', restauranteId);

    return this.http.get(`/api/reservas/porRestaurante`,{ params: params })
      .pipe(
        map(this.crearArregloReservas),

      );

  }

  getReservaByFecha(fecha){
    let params = new HttpParams();
    params = params.append('fecha', fecha);

    return this.http.get(`/api/reservas/porFecha`,{ params: params })
      .pipe(
        map(this.crearArregloReservas),

      );

  }

  getReservaByCliente(cedula){
    let params = new HttpParams();
    params = params.append('cedula', cedula);

    return this.http.get(`/api/reservas/porCliente`,{ params: params })
      .pipe(
        map(this.crearArregloReservas),

      );

  }



  
  
  

}
