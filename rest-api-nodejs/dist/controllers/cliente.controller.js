"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getClientes = getClientes;
exports.getOneCliente = getOneCliente;
exports.createCliente = createCliente;
exports.updateCliente = updateCliente;
exports.deleteCliente = deleteCliente;

var _Cliente = _interopRequireDefault(require("../models/Cliente"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function getClientes(_x, _x2) {
  return _getClientes.apply(this, arguments);
}

function _getClientes() {
  _getClientes = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var clientes;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _Cliente["default"].findAll();

          case 3:
            clientes = _context.sent;
            res.json({
              data: clientes
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Ocurrio un error al obtener clientes',
              data: {}
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getClientes.apply(this, arguments);
}

;

function getOneCliente(_x3, _x4) {
  return _getOneCliente.apply(this, arguments);
}

function _getOneCliente() {
  _getOneCliente = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var cedula, cliente;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            cedula = req.params.cedula;
            _context2.next = 4;
            return _Cliente["default"].findOne({
              where: {
                cedula: cedula
              }
            });

          case 4:
            cliente = _context2.sent;
            res.json(cliente);
            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: "Error al obtener cliente con id= " + id,
              data: {}
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 8]]);
  }));
  return _getOneCliente.apply(this, arguments);
}

;

function createCliente(_x5, _x6) {
  return _createCliente.apply(this, arguments);
}

function _createCliente() {
  _createCliente = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$body, cedula, nombre, apellido, newCliente;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _req$body = req.body, cedula = _req$body.cedula, nombre = _req$body.nombre, apellido = _req$body.apellido;
            _context3.next = 4;
            return _Cliente["default"].create({
              cedula: cedula,
              nombre: nombre,
              apellido: apellido
            }, {
              fields: ['cedula', 'nombre', 'apellido']
            });

          case 4:
            newCliente = _context3.sent;

            if (!newCliente) {
              _context3.next = 7;
              break;
            }

            return _context3.abrupt("return", res.json({
              message: 'Cliente created succesfully',
              data: newCliente
            }));

          case 7:
            _context3.next = 12;
            break;

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](0);
            res.status(400).json({
              message: 'Ha ocurrido un error al crear un cliente',
              data: {}
            });

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 9]]);
  }));
  return _createCliente.apply(this, arguments);
}

;

function updateCliente(_x7, _x8) {
  return _updateCliente.apply(this, arguments);
}

function _updateCliente() {
  _updateCliente = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _id, _req$body2, cedula, nombre, apellido, clientes;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _id = req.params.id;
            _req$body2 = req.body, cedula = _req$body2.cedula, nombre = _req$body2.nombre, apellido = _req$body2.apellido;
            _context5.next = 5;
            return _Cliente["default"].findAll({
              atribbutes: ['id', 'cedula', 'nombre', 'apellido'],
              where: {
                id: _id
              }
            });

          case 5:
            clientes = _context5.sent;

            if (clientes.length > 0) {
              clientes.forEach( /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(cliente) {
                  return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                      switch (_context4.prev = _context4.next) {
                        case 0:
                          _context4.next = 2;
                          return cliente.update({
                            cedula: cedula,
                            nombre: nombre,
                            apellido: apellido
                          });

                        case 2:
                        case "end":
                          return _context4.stop();
                      }
                    }
                  }, _callee4);
                }));

                return function (_x11) {
                  return _ref.apply(this, arguments);
                };
              }());
            }

            return _context5.abrupt("return", res.json({
              message: 'Cliente Updated Succesfully',
              data: clientes
            }));

          case 10:
            _context5.prev = 10;
            _context5.t0 = _context5["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al actualizar un cliente',
              data: {}
            });

          case 13:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 10]]);
  }));
  return _updateCliente.apply(this, arguments);
}

;

function deleteCliente(_x9, _x10) {
  return _deleteCliente.apply(this, arguments);
}

function _deleteCliente() {
  _deleteCliente = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var _id2, delteRowCount;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _id2 = req.params.id;
            _context6.next = 4;
            return _Cliente["default"].destroy({
              where: {
                id: _id2
              }
            });

          case 4:
            delteRowCount = _context6.sent;
            res.json({
              message: 'Cliente deleted successfully',
              count: delteRowCount
            });
            _context6.next = 11;
            break;

          case 8:
            _context6.prev = 8;
            _context6.t0 = _context6["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al eliminar un cliente',
              data: {}
            });

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 8]]);
  }));
  return _deleteCliente.apply(this, arguments);
}

;