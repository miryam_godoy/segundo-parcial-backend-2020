"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMesa = getMesa;
exports.getOneMesa = getOneMesa;
exports.createMesa = createMesa;
exports.updateMesa = updateMesa;
exports.deleteMesa = deleteMesa;
exports.getMesaDisponibles = getMesaDisponibles;

var _Mesa = _interopRequireDefault(require("../models/Mesa"));

var _Reserva = _interopRequireDefault(require("../models/Reserva"));

var _sequelize = _interopRequireDefault(require("sequelize"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function getMesa(_x, _x2) {
  return _getMesa.apply(this, arguments);
}

function _getMesa() {
  _getMesa = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var mesas;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _Mesa["default"].findAll();

          case 3:
            mesas = _context.sent;
            res.json({
              data: mesas
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Ocurrio un error al obtener las mesas',
              data: {}
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getMesa.apply(this, arguments);
}

;

function getOneMesa(_x3, _x4) {
  return _getOneMesa.apply(this, arguments);
}

function _getOneMesa() {
  _getOneMesa = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _id, mesa;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _id = req.params.id;
            _context2.next = 4;
            return _Mesa["default"].findOne({
              where: {
                id: _id
              }
            });

          case 4:
            mesa = _context2.sent;
            res.json(mesa);
            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: "Error al obtener mesa con id= " + id,
              data: {}
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 8]]);
  }));
  return _getOneMesa.apply(this, arguments);
}

;

function createMesa(_x5, _x6) {
  return _createMesa.apply(this, arguments);
}

function _createMesa() {
  _createMesa = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$body, nombre_mesa, restauranteid, posicion_x, posicion_y, nro_piso, cap_comensales, newMesa;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _req$body = req.body, nombre_mesa = _req$body.nombre_mesa, restauranteid = _req$body.restauranteid, posicion_x = _req$body.posicion_x, posicion_y = _req$body.posicion_y, nro_piso = _req$body.nro_piso, cap_comensales = _req$body.cap_comensales;
            _context3.next = 4;
            return _Mesa["default"].create({
              nombre_mesa: nombre_mesa,
              restauranteid: restauranteid,
              posicion_x: posicion_x,
              posicion_y: posicion_y,
              nro_piso: nro_piso,
              cap_comensales: cap_comensales
            }, {
              fields: ['nombre_mesa', 'restauranteid', 'posicion_x', 'posicion_y', 'nro_piso', 'cap_comensales']
            });

          case 4:
            newMesa = _context3.sent;

            if (!newMesa) {
              _context3.next = 7;
              break;
            }

            return _context3.abrupt("return", res.json({
              message: 'Mesa created succesfully',
              data: newMesa
            }));

          case 7:
            _context3.next = 12;
            break;

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](0);
            res.status(400).json({
              message: 'Ha ocurrido un error al crear una mesa',
              data: {}
            });

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 9]]);
  }));
  return _createMesa.apply(this, arguments);
}

;

function updateMesa(_x7, _x8) {
  return _updateMesa.apply(this, arguments);
}

function _updateMesa() {
  _updateMesa = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _id2, _req$body2, nombre_mesa, restauranteid, posicion_x, posicion_y, nro_piso, cap_comensales, mesas;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _id2 = req.params.id;
            _req$body2 = req.body, nombre_mesa = _req$body2.nombre_mesa, restauranteid = _req$body2.restauranteid, posicion_x = _req$body2.posicion_x, posicion_y = _req$body2.posicion_y, nro_piso = _req$body2.nro_piso, cap_comensales = _req$body2.cap_comensales;
            _context5.next = 5;
            return _Mesa["default"].findAll({
              atribbutes: ['id', 'nombre_mesa', 'restauranteid', 'posicion_x', 'posicion_y', 'nro_piso', 'cap_comensales'],
              where: {
                id: _id2
              }
            });

          case 5:
            mesas = _context5.sent;

            if (mesas.length > 0) {
              mesas.forEach( /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(mesa) {
                  return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                      switch (_context4.prev = _context4.next) {
                        case 0:
                          _context4.next = 2;
                          return mesa.update({
                            nombre_mesa: nombre_mesa,
                            restauranteid: restauranteid,
                            posicion_x: posicion_x,
                            posicion_y: posicion_y,
                            nro_piso: nro_piso,
                            cap_comensales: cap_comensales
                          });

                        case 2:
                        case "end":
                          return _context4.stop();
                      }
                    }
                  }, _callee4);
                }));

                return function (_x13) {
                  return _ref.apply(this, arguments);
                };
              }());
            }

            return _context5.abrupt("return", res.json({
              message: 'Mesa Updated Succesfully',
              data: mesas
            }));

          case 10:
            _context5.prev = 10;
            _context5.t0 = _context5["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al actualizar una mesa',
              data: {}
            });

          case 13:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 10]]);
  }));
  return _updateMesa.apply(this, arguments);
}

;

function deleteMesa(_x9, _x10) {
  return _deleteMesa.apply(this, arguments);
}

function _deleteMesa() {
  _deleteMesa = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var _id3, delteRowCount;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _id3 = req.params.id;
            _context6.next = 4;
            return _Mesa["default"].destroy({
              where: {
                id: _id3
              }
            });

          case 4:
            delteRowCount = _context6.sent;
            res.json({
              message: 'Mesa deleted successfully',
              count: delteRowCount
            });
            _context6.next = 11;
            break;

          case 8:
            _context6.prev = 8;
            _context6.t0 = _context6["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al eliminar una mesa',
              data: {}
            });

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 8]]);
  }));
  return _deleteMesa.apply(this, arguments);
}

;

function getMesaDisponibles(_x11, _x12) {
  return _getMesaDisponibles.apply(this, arguments);
}

function _getMesaDisponibles() {
  _getMesaDisponibles = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res) {
    var _req$body3, restaurante, fecha, hora_inicio, hora_fin, mesas, reservas, j, i;

    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            _req$body3 = req.body, restaurante = _req$body3.restaurante, fecha = _req$body3.fecha, hora_inicio = _req$body3.hora_inicio, hora_fin = _req$body3.hora_fin;
            console.log(fecha);
            _context7.next = 5;
            return _Mesa["default"].findAll({
              where: {
                restauranteid: restaurante
              }
            });

          case 5:
            mesas = _context7.sent;
            _context7.next = 8;
            return _Reserva["default"].findAll({
              where: {
                restaurante: restaurante
              }
            });

          case 8:
            reservas = _context7.sent;

            // console.log(mesas);
            //console.log(reservas)
            if (reservas.length > 0) {
              j = 0;

              for (i = 0; i < reservas.length; i++) {
                if (mesas[j].dataValues.restauranteid === reservas[i].dataValues.restaurante) {
                  if (reservas[i].dataValues.fecha === fecha && reservas[i].dataValues.hora_inicio.localeCompare(hora_inicio) === 0 && reservas[i].dataValues.hora_fin.localeCompare(hora_fin) === 0) {
                    mesas.splice(j, 1);
                  }
                }

                j++;
              }
            }

            res.json({
              data: mesas
            });
            _context7.next = 16;
            break;

          case 13:
            _context7.prev = 13;
            _context7.t0 = _context7["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al obtener mesas disponibles',
              data: {}
            });

          case 16:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 13]]);
  }));
  return _getMesaDisponibles.apply(this, arguments);
}

;