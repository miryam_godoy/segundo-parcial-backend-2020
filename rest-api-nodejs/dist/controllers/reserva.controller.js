"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getReservas = getReservas;
exports.getReservaPorFecha = getReservaPorFecha;
exports.getReservaRestaurante = getReservaRestaurante;
exports.getReservaCliente = getReservaCliente;
exports.createReserva = createReserva;
exports.updateReserva = updateReserva;
exports.deleteReserva = deleteReserva;

var _Cliente = _interopRequireDefault(require("../models/Cliente"));

var _Mesa = _interopRequireDefault(require("../models/Mesa"));

var _Reserva = _interopRequireDefault(require("../models/Reserva"));

var _Restaurante = _interopRequireDefault(require("../models/Restaurante"));

var _sequelize = _interopRequireDefault(require("sequelize"));

var _moment = _interopRequireDefault(require("moment"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function getReservas(_x, _x2) {
  return _getReservas.apply(this, arguments);
}

function _getReservas() {
  _getReservas = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var reservas;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _Reserva["default"].findAll({
              attributes: {
                exclude: ['restaurante', 'mesa', 'cliente']
              },
              include: [{
                model: _Restaurante["default"],
                as: 'Restaurante' //here is goes the alias of the association

              }, {
                model: _Mesa["default"],
                as: 'Mesa'
              }, {
                model: _Cliente["default"],
                as: 'Cliente'
              }]
            });

          case 3:
            reservas = _context.sent;
            res.json({
              data: reservas
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Ocurrio un error al obtener las reservaciones',
              data: {}
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getReservas.apply(this, arguments);
}

;

function getReservaPorFecha(_x3, _x4) {
  return _getReservaPorFecha.apply(this, arguments);
}

function _getReservaPorFecha() {
  _getReservaPorFecha = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _fecha, reserva;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _fecha = req.query.fecha; // const fecha = fecha1.toISOString().split('T')[0];
            // console.log(fecha1.toISOString().split('T')[0]);

            _context2.next = 4;
            return _Reserva["default"].findAll({
              attributes: {
                exclude: ['restaurante', 'mesa', 'cliente']
              },
              include: [{
                model: _Restaurante["default"],
                as: 'Restaurante' //here is goes the alias of the association

              }, {
                model: _Mesa["default"],
                as: 'Mesa'
              }, {
                model: _Cliente["default"],
                as: 'Cliente'
              }],
              where: {
                fecha: _fecha
              },
              atribbutes: ['id', 'restaurante', 'mesa', 'fecha', 'hora', 'cliente', 'cantidad'],
              order: [['mesa', 'DESC'], ['hora', 'DESC']]
            });

          case 4:
            reserva = _context2.sent;
            res.json(reserva);
            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: "Error al obtener reserva con fecha= " + fecha,
              data: {}
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 8]]);
  }));
  return _getReservaPorFecha.apply(this, arguments);
}

;

function getReservaRestaurante(_x5, _x6) {
  return _getReservaRestaurante.apply(this, arguments);
}

function _getReservaRestaurante() {
  _getReservaRestaurante = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var _restaurante, reserva;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _restaurante = req.query.restaurante; // const fecha = fecha1.toISOString().split('T')[0];
            // console.log(fecha1.toISOString().split('T')[0]);

            _context3.next = 4;
            return _Reserva["default"].findAll({
              attributes: {
                exclude: ['restaurante', 'mesa', 'cliente']
              },
              include: [{
                model: _Restaurante["default"],
                as: 'Restaurante' //here is goes the alias of the association

              }, {
                model: _Mesa["default"],
                as: 'Mesa'
              }, {
                model: _Cliente["default"],
                as: 'Cliente'
              }],
              where: {
                restaurante: _restaurante
              },
              atribbutes: ['id', 'restaurante', 'mesa', 'fecha', 'hora', 'cliente', 'cantidad'],
              order: [['mesa', 'DESC'], ['hora', 'DESC']]
            });

          case 4:
            reserva = _context3.sent;
            res.json(reserva);
            _context3.next = 12;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](0);
            console.log(_context3.t0);
            res.status(500).json({
              message: "Error al obtener reserva con restaurante= " + restaurante,
              data: {}
            });

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 8]]);
  }));
  return _getReservaRestaurante.apply(this, arguments);
}

;

function getReservaCliente(_x7, _x8) {
  return _getReservaCliente.apply(this, arguments);
}

function _getReservaCliente() {
  _getReservaCliente = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
    var _cliente, reserva;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _cliente = req.query.cliente; // const fecha = fecha1.toISOString().split('T')[0];
            // console.log(fecha1.toISOString().split('T')[0]);

            _context4.next = 4;
            return _Reserva["default"].findAll({
              attributes: {
                exclude: ['restaurante', 'mesa', 'cliente']
              },
              include: [{
                model: _Restaurante["default"],
                as: 'Restaurante' //here is goes the alias of the association

              }, {
                model: _Mesa["default"],
                as: 'Mesa'
              }, {
                model: _Cliente["default"],
                as: 'Cliente'
              }],
              where: {
                cliente: _cliente
              },
              atribbutes: ['id', 'restaurante', 'mesa', 'fecha', 'hora', 'cliente', 'cantidad'],
              order: [['mesa', 'DESC'], ['hora', 'DESC']]
            });

          case 4:
            reserva = _context4.sent;
            res.json(reserva);
            _context4.next = 12;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);
            res.status(500).json({
              message: "Error al obtener reserva con restaurante= " + cliente,
              data: {}
            });

          case 12:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 8]]);
  }));
  return _getReservaCliente.apply(this, arguments);
}

;

function createReserva(_x9, _x10) {
  return _createReserva.apply(this, arguments);
}

function _createReserva() {
  _createReserva = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _req$body, _restaurante2, mesa, _fecha2, hora_inicio, hora_fin, _cliente2, cantidad, newReserva;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            console.log("puto");
            _context5.prev = 1;
            _req$body = req.body, _restaurante2 = _req$body.restaurante, mesa = _req$body.mesa, _fecha2 = _req$body.fecha, hora_inicio = _req$body.hora_inicio, hora_fin = _req$body.hora_fin, _cliente2 = _req$body.cliente, cantidad = _req$body.cantidad;
            _context5.next = 5;
            return _Reserva["default"].create({
              restaurante: _restaurante2,
              mesa: mesa,
              fecha: _fecha2,
              hora_inicio: hora_inicio,
              hora_fin: hora_fin,
              cliente: _cliente2,
              cantidad: cantidad
            }, {
              fields: ['restaurante', 'mesa', 'fecha', 'hora_inicio', 'hora_fin', 'cliente', 'cantidad']
            });

          case 5:
            newReserva = _context5.sent;

            if (!newReserva) {
              _context5.next = 8;
              break;
            }

            return _context5.abrupt("return", res.json({
              message: 'Reserva created succesfully',
              data: newReserva
            }));

          case 8:
            _context5.next = 13;
            break;

          case 10:
            _context5.prev = 10;
            _context5.t0 = _context5["catch"](1);
            res.status(400).json({
              message: 'Ha ocurrido un error al crear una reserva',
              data: {}
            });

          case 13:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[1, 10]]);
  }));
  return _createReserva.apply(this, arguments);
}

;

function updateReserva(_x11, _x12) {
  return _updateReserva.apply(this, arguments);
}

function _updateReserva() {
  _updateReserva = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res) {
    var id, _req$body2, _fecha3, hora, reservas;

    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            id = req.params.id;
            _req$body2 = req.body, _fecha3 = _req$body2.fecha, hora = _req$body2.hora;
            _context7.next = 5;
            return _Reserva["default"].findAll({
              atribbutes: ['id', 'fecha', 'hora'],
              where: {
                id: id
              }
            });

          case 5:
            reservas = _context7.sent;

            if (reservas.length > 0) {
              reservas.forEach( /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(reserva) {
                  return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                      switch (_context6.prev = _context6.next) {
                        case 0:
                          _context6.next = 2;
                          return reserva.update({
                            fecha: _fecha3,
                            hora: hora
                          });

                        case 2:
                        case "end":
                          return _context6.stop();
                      }
                    }
                  }, _callee6);
                }));

                return function (_x15) {
                  return _ref.apply(this, arguments);
                };
              }());
            }

            return _context7.abrupt("return", res.json({
              message: 'Reserva Updated Succesfully',
              data: reservas
            }));

          case 10:
            _context7.prev = 10;
            _context7.t0 = _context7["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al actualizar un cliente',
              data: {}
            });

          case 13:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 10]]);
  }));
  return _updateReserva.apply(this, arguments);
}

;

function deleteReserva(_x13, _x14) {
  return _deleteReserva.apply(this, arguments);
}

function _deleteReserva() {
  _deleteReserva = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res) {
    var id, delteRowCount;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            id = req.params.id;
            _context8.next = 4;
            return _Reserva["default"].destroy({
              where: {
                id: id
              }
            });

          case 4:
            delteRowCount = _context8.sent;
            res.json({
              message: 'Reserva deleted successfully',
              count: delteRowCount
            });
            _context8.next = 11;
            break;

          case 8:
            _context8.prev = 8;
            _context8.t0 = _context8["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al eliminar una reserva',
              data: {}
            });

          case 11:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 8]]);
  }));
  return _deleteReserva.apply(this, arguments);
}

;