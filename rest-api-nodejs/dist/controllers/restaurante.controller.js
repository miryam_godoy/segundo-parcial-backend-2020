"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRestaurante = getRestaurante;
exports.getOneRestaurante = getOneRestaurante;
exports.createRestaurante = createRestaurante;
exports.updateRestaurante = updateRestaurante;
exports.deleteRestaurante = deleteRestaurante;

var _Restaurante = _interopRequireDefault(require("../models/Restaurante"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function getRestaurante(_x, _x2) {
  return _getRestaurante.apply(this, arguments);
}

function _getRestaurante() {
  _getRestaurante = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var restaurantes;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _Restaurante["default"].findAll();

          case 3:
            restaurantes = _context.sent;
            res.json({
              data: restaurantes
            });
            _context.next = 11;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            res.status(500).json({
              message: 'Ocurrio un error al obtener los restaurantes',
              data: {}
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 7]]);
  }));
  return _getRestaurante.apply(this, arguments);
}

;

function getOneRestaurante(_x3, _x4) {
  return _getOneRestaurante.apply(this, arguments);
}

function _getOneRestaurante() {
  _getOneRestaurante = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _id, restaurante;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _id = req.params.id;
            _context2.next = 4;
            return _Restaurante["default"].findOne({
              where: {
                id: _id
              }
            });

          case 4:
            restaurante = _context2.sent;
            res.json(restaurante);
            _context2.next = 12;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);
            res.status(500).json({
              message: "Error al obtener restaurante con id= " + id,
              data: {}
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 8]]);
  }));
  return _getOneRestaurante.apply(this, arguments);
}

;

function createRestaurante(_x5, _x6) {
  return _createRestaurante.apply(this, arguments);
}

function _createRestaurante() {
  _createRestaurante = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var _req$body, nombre, direccion, newRestaurante;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _req$body = req.body, nombre = _req$body.nombre, direccion = _req$body.direccion;
            _context3.prev = 1;
            _context3.next = 4;
            return _Restaurante["default"].create({
              nombre: nombre,
              direccion: direccion
            }, {
              fields: ['nombre', 'direccion']
            });

          case 4:
            newRestaurante = _context3.sent;

            if (!newRestaurante) {
              _context3.next = 7;
              break;
            }

            return _context3.abrupt("return", res.json({
              message: 'Restaurante created succesfully',
              data: newRestaurante
            }));

          case 7:
            _context3.next = 12;
            break;

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](1);
            res.status(400).json({
              message: 'Ha ocurrido un error al crear un restaurante',
              data: {}
            });

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[1, 9]]);
  }));
  return _createRestaurante.apply(this, arguments);
}

;

function updateRestaurante(_x7, _x8) {
  return _updateRestaurante.apply(this, arguments);
}

function _updateRestaurante() {
  _updateRestaurante = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
    var _id2, _req$body2, nombre, direccion, restaurantes;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _id2 = req.params.id;
            _req$body2 = req.body, nombre = _req$body2.nombre, direccion = _req$body2.direccion;
            _context5.next = 5;
            return _Restaurante["default"].findAll({
              atribbutes: ['id', 'nombre', 'direccion'],
              where: {
                id: _id2
              }
            });

          case 5:
            restaurantes = _context5.sent;

            if (restaurantes.length > 0) {
              restaurantes.forEach( /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(restaurante) {
                  return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                      switch (_context4.prev = _context4.next) {
                        case 0:
                          _context4.next = 2;
                          return restaurante.update({
                            nombre: nombre,
                            direccion: direccion
                          });

                        case 2:
                        case "end":
                          return _context4.stop();
                      }
                    }
                  }, _callee4);
                }));

                return function (_x11) {
                  return _ref.apply(this, arguments);
                };
              }());
            }

            return _context5.abrupt("return", res.json({
              message: 'Restaurante Updated Succesfully',
              data: restaurantes
            }));

          case 10:
            _context5.prev = 10;
            _context5.t0 = _context5["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al actualizar un restaurante',
              data: {}
            });

          case 13:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 10]]);
  }));
  return _updateRestaurante.apply(this, arguments);
}

;

function deleteRestaurante(_x9, _x10) {
  return _deleteRestaurante.apply(this, arguments);
}

function _deleteRestaurante() {
  _deleteRestaurante = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
    var _id3, delteRowCount;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _id3 = req.params.id;
            _context6.next = 4;
            return _Restaurante["default"].destroy({
              where: {
                id: _id3
              }
            });

          case 4:
            delteRowCount = _context6.sent;
            res.json({
              message: 'Restaurante deleted successfully',
              count: delteRowCount
            });
            _context6.next = 11;
            break;

          case 8:
            _context6.prev = 8;
            _context6.t0 = _context6["catch"](0);
            res.status(500).json({
              message: 'Ha ocurrido un error al eliminar un restaurante',
              data: {}
            });

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 8]]);
  }));
  return _deleteRestaurante.apply(this, arguments);
}

;