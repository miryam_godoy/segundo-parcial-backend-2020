"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

var _Reserva = _interopRequireDefault(require("./Reserva"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Cliente = _database.sequelize.define('clientes', {
  id: {
    type: _sequelize["default"].INTEGER,
    primaryKey: true
  },
  cedula: {
    type: _sequelize["default"].INTEGER
  },
  nombre: {
    type: _sequelize["default"].TEXT
  },
  apellido: {
    type: _sequelize["default"].TEXT
  }
}, {
  freezeTableName: true,
  timestamps: false
});

Cliente.hasMany(_Reserva["default"], {
  foreignKey: 'cliente',
  targetKey: 'id'
});

_Reserva["default"].belongsTo(Cliente, {
  foreignKey: 'cliente',
  as: 'Cliente',
  targetKey: 'id'
});

var _default = Cliente;
exports["default"] = _default;