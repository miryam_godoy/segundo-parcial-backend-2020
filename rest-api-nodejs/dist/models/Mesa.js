"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

var _Reserva = _interopRequireDefault(require("./Reserva"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Mesa = _database.sequelize.define('mesa', {
  id: {
    type: _sequelize["default"].INTEGER,
    primaryKey: true
  },
  nombre_mesa: {
    type: _sequelize["default"].TEXT
  },
  restauranteid: {
    type: _sequelize["default"].INTEGER
  },
  posicion_x: {
    type: _sequelize["default"].INTEGER
  },
  posicion_y: {
    type: _sequelize["default"].INTEGER
  },
  nro_piso: {
    type: _sequelize["default"].INTEGER
  },
  cap_comensales: {
    type: _sequelize["default"].INTEGER
  }
}, {
  freezeTableName: true,
  timestamps: false
}); //Mesa.hasMany(Reserva, { foreignKey: 'mesa', as: 'MesaId', sourceKey: 'id' });
//Reserva.belongsTo(Mesa, { foreignKey: 'mesa',  sourceKey: 'id' } );


Mesa.hasMany(_Reserva["default"], {
  foreignKey: 'mesa',
  targetKey: 'id'
});

_Reserva["default"].belongsTo(Mesa, {
  foreignKey: 'mesa',
  as: 'Mesa',
  targetKey: 'id'
});

var _default = Mesa;
exports["default"] = _default;