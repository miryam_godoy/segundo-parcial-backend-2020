"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//import Cliente from './Cliente';
//import Mesa from './Mesa';
//import Restaurante from './Restaurante';
var Reserva = _database.sequelize.define('reservas', {
  id: {
    type: _sequelize["default"].INTEGER,
    primaryKey: true
  },
  restaurante: {
    type: _sequelize["default"].INTEGER
  },
  mesa: {
    type: _sequelize["default"].INTEGER
  },
  fecha: {
    type: _sequelize["default"].DATE
  },
  hora_inicio: {
    type: _sequelize["default"].TEXT
  },
  hora_fin: {
    type: _sequelize["default"].TEXT
  },
  cliente: {
    type: _sequelize["default"].INTEGER
  },
  cantidad: {
    type: _sequelize["default"].INTEGER
  }
}, {
  freezeTableName: true,
  timestamps: false
}); //import Cliente from './Cliente';


var _default = Reserva;
exports["default"] = _default;