"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _database = require("../database/database");

var _Mesa = _interopRequireDefault(require("./Mesa"));

var _Reserva = _interopRequireDefault(require("./Reserva"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Restaurante = _database.sequelize.define('restaurante', {
  id: {
    type: _sequelize["default"].INTEGER,
    primaryKey: true
  },
  nombre: {
    type: _sequelize["default"].TEXT
  },
  direccion: {
    type: _sequelize["default"].TEXT
  }
}, {
  freezeTableName: true,
  timestamps: false
});

Restaurante.hasMany(_Mesa["default"], {
  foreignKey: 'restauranteid',
  sourceKey: 'id'
});

_Mesa["default"].belongsTo(Restaurante, {
  foreignKey: 'restauranteid',
  sourceKey: 'id'
});

Restaurante.hasMany(_Reserva["default"], {
  foreignKey: 'restaurante',
  targetKey: 'id'
});

_Reserva["default"].belongsTo(Restaurante, {
  foreignKey: 'restaurante',
  as: 'Restaurante',
  targetKey: 'id'
});

var _default = Restaurante;
exports["default"] = _default;