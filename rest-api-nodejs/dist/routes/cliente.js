"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _cliente = require("../controllers/cliente.controller");

var router = (0, _express.Router)();
// /api/clientes/
router.post('/', _cliente.createCliente);
router.get('/', _cliente.getClientes); // /api/projects/projectID

router.get('/:cedula', _cliente.getOneCliente);
router["delete"]('/:id', _cliente.deleteCliente);
router.put('/:id', _cliente.updateCliente);
var _default = router;
exports["default"] = _default;