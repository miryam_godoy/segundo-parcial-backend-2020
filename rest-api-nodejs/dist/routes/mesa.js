"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _mesa = require("../controllers/mesa.controller");

var router = (0, _express.Router)();
// /api/mesas/
router.post('/', _mesa.createMesa);
router.get('/', _mesa.getMesa); // /api/mesas/mesasDisponibles

router.post('/mesasDisponibles', _mesa.getMesaDisponibles); // /api/projects/projectID

router.get('/:id', _mesa.getOneMesa);
router["delete"]('/:id', _mesa.deleteMesa);
router.put('/:id', _mesa.updateMesa);
var _default = router;
exports["default"] = _default;