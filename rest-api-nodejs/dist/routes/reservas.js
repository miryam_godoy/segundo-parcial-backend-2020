"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _reserva = require("../controllers/reserva.controller");

var router = (0, _express.Router)();
// /api/reservas/
router.post('/addReserva', _reserva.createReserva);
router.get('/', _reserva.getReservas); // /api/reservas/porFecha?fecha={fecha}

router.get('/porFecha', _reserva.getReservaPorFecha); // /api/reservas/porRestaurante?restaurante={idRestaurante}

router.get('/porRestaurante', _reserva.getReservaRestaurante); // /api/reservas/porCliente?cliente={idCliente}

router.get('/porCliente', _reserva.getReservaCliente); // /api/reservas/mesasDisponibles/
//router.get('/mesasDisponibles', getMesasDisponibles);
// /api/restaurantes/restauranteID
//router.get('/:id', getOneRestaurante);

router["delete"]('/:id', _reserva.deleteReserva); //router.put('/:id', updateRestaurante)

var _default = router;
exports["default"] = _default;