"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _restaurante = require("../controllers/restaurante.controller");

var router = (0, _express.Router)();
// /api/restaurantes/
router.get('/', _restaurante.getRestaurante);
router.post('/', _restaurante.createRestaurante); // /api/restaurantes/restauranteID

router.get('/:id', _restaurante.getOneRestaurante);
router["delete"]('/:id', _restaurante.deleteRestaurante);
router.put('/:id', _restaurante.updateRestaurante);
var _default = router;
exports["default"] = _default;