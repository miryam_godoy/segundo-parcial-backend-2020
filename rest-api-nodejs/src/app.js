import express, { json } from 'express';
import morgan from 'morgan';

// importing routes
import restauranteRouters from './routes/restaurante';
import mesaRouters from './routes/mesa';
import clienteRouters from './routes/cliente';
import reservaRouters from './routes/reservas';


//initialization
const app = express();

// middlewares
app.use(morgan('dev'));
app.use(json());

//routes
app.use('/api/restaurantes', restauranteRouters);
app.use('/api/mesas', mesaRouters);
app.use('/api/clientes', clienteRouters);
app.use('/api/reservas', reservaRouters);


export default app;