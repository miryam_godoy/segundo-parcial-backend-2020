import Cliente from '../models/Cliente';

export async function getClientes(req, res) {
    try {
        const clientes = await Cliente.findAll();
        res.json({
            data: clientes
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Ocurrio un error al obtener clientes',
            data: {}
        });
    }

};

export async function getOneCliente(req, res) {
    try {
        const { cedula } = req.params;
        const cliente = await Cliente.findOne({
            where: {
                cedula
            }
        });
        res.json(cliente);
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: "Error al obtener cliente con id= " + id,
            data: {}
        });
    }

};

export async function createCliente(req, res) {
    try {

        const { cedula, nombre, apellido } = req.body;

        let newCliente = await Cliente.create({
            cedula,
            nombre,
            apellido
        }, {
            fields: ['cedula', 'nombre', 'apellido']
        });
        if (newCliente) {
            return res.json({
                message: 'Cliente created succesfully',
                data: newCliente
            });
        }

    } catch (e) {
        res.status(400).json({
            message: 'Ha ocurrido un error al crear un cliente',
            data: {}
        });
    }
};

export async function updateCliente(req, res) {
    try {

        const { id } = req.params;
        const { cedula, nombre, apellido } = req.body;

        const clientes = await Cliente.findAll({
            atribbutes: ['id', 'cedula', 'nombre', 'apellido'],
            where: {
                id
            }
        });

        if (clientes.length > 0) {

            clientes.forEach(async cliente => {
                await cliente.update({
                    cedula,
                    nombre,
                    apellido
                })
            })

        }
        return res.json({
            message: 'Cliente Updated Succesfully',
            data: clientes
        });

    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al actualizar un cliente',
            data: {}
        });
    }

};

export async function deleteCliente(req, res) {

    try {

        const { id } = req.params;
        const delteRowCount = await Cliente.destroy({
            where: {
                id
            }
        });
        res.json({
            message: 'Cliente deleted successfully',
            count: delteRowCount
        });

    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al eliminar un cliente',
            data: {}
        });

    }

};