import Mesa from '../models/Mesa';
import Reserva from '../models/Reserva';
import Op from 'sequelize';


export async function getMesa(req, res) {
    try {
        const mesas = await Mesa.findAll();
        res.json({
            data: mesas
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Ocurrio un error al obtener las mesas',
            data: {}
        });
    }

};

export async function getOneMesa(req, res) {
    try {
        const { id } = req.params;
        const mesa = await Mesa.findOne({
            where: {
                id
            }
        });
        res.json(mesa);
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: "Error al obtener mesa con id= " + id,
            data: {}
        });
    }

};

export async function createMesa(req, res) {
    try {

        const { nombre_mesa, restauranteid, posicion_x, posicion_y, nro_piso, cap_comensales } = req.body;

        let newMesa = await Mesa.create({
            nombre_mesa,
            restauranteid,
            posicion_x,
            posicion_y,
            nro_piso,
            cap_comensales
        }, {
            fields: ['nombre_mesa', 'restauranteid', 'posicion_x', 'posicion_y', 'nro_piso', 'cap_comensales']
        });
        if (newMesa) {
            return res.json({
                message: 'Mesa created succesfully',
                data: newMesa
            });
        }

    } catch (e) {
        res.status(400).json({
            message: 'Ha ocurrido un error al crear una mesa',
            data: {}
        });
    }
};


export async function updateMesa(req, res) {
    try {

        const { id } = req.params;
        const { nombre_mesa, restauranteid, posicion_x, posicion_y, nro_piso, cap_comensales } = req.body;

        const mesas = await Mesa.findAll({
            atribbutes: ['id', 'nombre_mesa', 'restauranteid', 'posicion_x', 'posicion_y', 'nro_piso', 'cap_comensales'],
            where: {
                id
            }
        });

        if (mesas.length > 0) {

            mesas.forEach(async mesa => {
                await mesa.update({
                    nombre_mesa,
                    restauranteid,
                    posicion_x,
                    posicion_y,
                    nro_piso,
                    cap_comensales
                })
            })

        }
        return res.json({
            message: 'Mesa Updated Succesfully',
            data: mesas
        });

    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al actualizar una mesa',
            data: {}
        });
    }

};

export async function deleteMesa(req, res) {

    try {

        const { id } = req.params;
        const delteRowCount = await Mesa.destroy({
            where: {
                id
            }
        });
        res.json({
            message: 'Mesa deleted successfully',
            count: delteRowCount
        });

    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al eliminar una mesa',
            data: {}
        });

    }

};

export async function getMesaDisponibles(req, res) {

    try {
        const { restaurante, fecha, hora_inicio, hora_fin } = req.body;
        console.log(fecha);
        const mesas = await Mesa.findAll({
            where: {
                restauranteid: restaurante
            }
        });
        const reservas = await Reserva.findAll({
            where: {
                restaurante
            }
        });
        // console.log(mesas);
        console.log(reservas.length);
        console.log(mesas.length)

        let tamMesas = mesas.length;

        if (reservas.length > 0) {
            for (let j = 0; j < mesas.length; j++) {
                for (let i = 0; i < reservas.length; i++) {

                    if (mesas[j].dataValues.restauranteid === reservas[i].dataValues.restaurante) {
                        if (reservas[i].dataValues.fecha === fecha &&
                            reservas[i].dataValues.hora_inicio.localeCompare(hora_inicio) === 0 &&
                            reservas[i].dataValues.hora_fin.localeCompare(hora_fin) === 0) {
                            mesas.splice(j, 1);
                            break;
                        }
                    }
                }

            }



        }

        res.json({
            data: mesas
        });

    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al obtener mesas disponibles',
            data: {}
        });
    }


};