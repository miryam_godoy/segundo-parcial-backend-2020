import Cliente from '../models/Cliente';
import Mesa from '../models/Mesa';
import Reserva from '../models/Reserva';
import Restaurante from '../models/Restaurante';
import Op, { where } from 'sequelize';
import moment from 'moment';
import { sequelize } from '../database/database';

export async function getReservas(req, res) {
    try {
        const reservas = await Reserva.findAll({
            attributes: {
                exclude: ['restaurante', 'mesa', 'cliente']
            },
            include: [{
                model: Restaurante,
                as: 'Restaurante' //here is goes the alias of the association
            }, {
                model: Mesa,
                as: 'Mesa'
            }, {
                model: Cliente,
                as: 'Cliente'
            }],

        });
        res.json({
            data: reservas
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: 'Ocurrio un error al obtener las reservaciones',
            data: {}
        });
    }

};

export async function getReservaPorFecha(req, res) {
    try {
        const fecha = req.query.fecha;
        // const fecha = fecha1.toISOString().split('T')[0];
        // console.log(fecha1.toISOString().split('T')[0]);
        const reserva = await Reserva.findAll({
            attributes: {
                exclude: ['restaurante', 'mesa', 'cliente']
            },
            include: [{
                model: Restaurante,
                as: 'Restaurante' //here is goes the alias of the association
            }, {
                model: Mesa,
                as: 'Mesa'
            }, {
                model: Cliente,
                as: 'Cliente'
            }],
            where: {
                fecha,
            },
            atribbutes: ['id', 'restaurante', 'mesa', 'fecha', 'hora', 'cliente', 'cantidad'],
            order: [
                ['fecha', 'DESC'],
                // ['hora', 'DESC']
            ]


        });
        res.json({
            data: reserva
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: "Error al obtener reserva con fecha= " + fecha,
            data: {}
        });
    }

};

export async function getReservaRestaurante(req, res) {
    try {
        const restaurante = req.query.restaurante;
        // const fecha = fecha1.toISOString().split('T')[0];
        // console.log(fecha1.toISOString().split('T')[0]);
        const reserva = await Reserva.findAll({
            attributes: {
                exclude: ['restaurante', 'mesa', 'cliente']
            },
            include: [{
                model: Restaurante,
                as: 'Restaurante' //here is goes the alias of the association
            }, {
                model: Mesa,
                as: 'Mesa'
            }, {
                model: Cliente,
                as: 'Cliente'
            }],
            where: {
                restaurante,
            },
            atribbutes: ['id', 'restaurante', 'mesa', 'fecha', 'hora', 'cliente', 'cantidad'],
            order: [
                ['mesa', 'DESC'],
                // ['hora', 'DESC']
            ],


        });
        res.json({
            data: reserva
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: "Error al obtener reserva con restaurante= " + restaurante,
            data: {}
        });
    }

};

export async function getReservaCliente(req, res) {
    try {
        const cedula = req.query.cedula;
        // const fecha = fecha1.toISOString().split('T')[0];
        // console.log(fecha1.toISOString().split('T')[0]);
        const reserva = await Reserva.findAll({
            attributes: {
                exclude: ['restaurante', 'mesa', 'cliente']
            },

            include: [{
                    model: Cliente,
                    as: 'Cliente',
                    where: {
                        cedula
                    },
                    required: false,
                },
                {
                    model: Restaurante,
                    as: 'Restaurante' //here is goes the alias of the association
                }, {
                    model: Mesa,
                    as: 'Mesa'
                }
            ],
            atribbutes: ['id', 'restaurante', 'mesa', 'fecha', 'hora', 'cliente', 'cantidad'],
            order: [
                ['mesa', 'DESC'],
                // ['hora', 'DESC']
            ],


        });
        res.json(reserva);
    } catch (e) {
        console.log(e);
        res.status(500).json({
            message: "Error al obtener reserva con restaurante= " + cliente,
            data: {}
        });
    }

};


export async function createReserva(req, res) {
    console.log("puto");
    try {

        const { restaurante, mesa, fecha, hora_inicio, hora_fin, cliente, cantidad } = req.body;


        let newReserva = await Reserva.create({
            restaurante,
            mesa,
            fecha,
            hora_inicio,
            hora_fin,
            cliente,
            cantidad
        }, {
            fields: ['restaurante', 'mesa', 'fecha', 'hora_inicio', 'hora_fin', 'cliente', 'cantidad']
        });

        if (newReserva) {
            return res.json({
                message: 'Reserva created succesfully',
                data: newReserva
            });
        }

    } catch (e) {
        res.status(400).json({
            message: 'Ha ocurrido un error al crear una reserva',
            data: {}
        });
    }
};

export async function updateReserva(req, res) {
    try {

        const { id } = req.params;
        const { fecha, hora } = req.body;

        const reservas = await Reserva.findAll({
            atribbutes: ['id', 'fecha', 'hora'],
            where: {
                id
            }
        });

        if (reservas.length > 0) {

            reservas.forEach(async reserva => {
                await reserva.update({
                    fecha,
                    hora
                })
            })

        }
        return res.json({
            message: 'Reserva Updated Succesfully',
            data: reservas
        });

    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al actualizar un cliente',
            data: {}
        });
    }

};


export async function deleteReserva(req, res) {

    try {

        const { id } = req.params;
        const delteRowCount = await Reserva.destroy({
            where: {
                id
            }
        });
        res.json({
            message: 'Reserva deleted successfully',
            count: delteRowCount
        });

    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al eliminar una reserva',
            data: {}
        });

    }

};