import Restaurante from '../models/Restaurante';


export async function getRestaurante(req, res){
    try{
        const restaurantes = await Restaurante.findAll();
        res.json({
            data: restaurantes
        });
    }catch(e){
        console.log(e);
        res.status(500).json({
            message: 'Ocurrio un error al obtener los restaurantes',
            data: {}
        });
    }
   
};

export async function getOneRestaurante(req, res){
    try{
        const { id } = req.params;
        const restaurante = await Restaurante.findOne({
        where: {
            id
        }
    });
    res.json(restaurante);
    }catch(e){
        console.log(e);
        res.status(500).json({
            message: "Error al obtener restaurante con id= " + id,
            data: {}
        });
    }
    
};


export async function createRestaurante(req, res){
    const { nombre, direccion } = req.body;
    try{

        let newRestaurante = await Restaurante.create({
           nombre,
           direccion
        }, {
            fields: [ 'nombre', 'direccion' ]
        });
        if(newRestaurante){
            return res.json({
                message: 'Restaurante created succesfully',
                data: newRestaurante
            });
        }

    }catch(e){
        res.status(400).json({
            message: 'Ha ocurrido un error al crear un restaurante',
            data: {}
        });
    }
};


export async function updateRestaurante(req, res){
    try {

        const { id } = req.params;
        const { nombre, direccion } = req.body;
    
        const restaurantes = await Restaurante.findAll({
            atribbutes: ['id', 'nombre', 'direccion'],
            where:{
                id
            }
        });
    
        if(restaurantes.length > 0){
    
            restaurantes.forEach(async restaurante => {
               await restaurante.update({
                   nombre,
                   direccion,
               })
            })
    
        }
        return res.json({
            message: 'Restaurante Updated Succesfully',
            data: restaurantes
        });
        
    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al actualizar un restaurante',
            data: {}
        });
    }

};

export async function deleteRestaurante(req,res){

    try {

        const { id } = req.params;
        const delteRowCount = await Restaurante.destroy({
        where: {
            id
        }
         });
         res.json({
            message: 'Restaurante deleted successfully',
            count: delteRowCount
        });
        
    } catch (error) {
        res.status(500).json({
            message: 'Ha ocurrido un error al eliminar un restaurante',
            data: {}
        });
        
    }
    
};

