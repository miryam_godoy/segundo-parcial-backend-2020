import Sequelize from 'sequelize';
import { sequelize } from '../database/database';


const Cliente = sequelize.define('clientes', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    cedula: {
        type: Sequelize.INTEGER
    },
    nombre: {
        type: Sequelize.TEXT
    },
    apellido: {
        type: Sequelize.TEXT
    }
}, {
    freezeTableName: true,
    timestamps: false
});

import Reserva from './Reserva';


Cliente.hasMany(Reserva, { foreignKey: 'cliente', targetKey: 'id'});
Reserva.belongsTo(Cliente, { foreignKey: 'cliente', as: 'Cliente', targetKey: 'id'});

export default Cliente;