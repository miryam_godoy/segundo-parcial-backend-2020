import Sequelize from 'sequelize';
import { sequelize } from '../database/database';
import Reserva from './Reserva';


const Mesa = sequelize.define('mesa',{
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    nombre_mesa: {
        type: Sequelize.TEXT
    },
    restauranteid: {
        type: Sequelize.INTEGER,
    },
    posicion_x: {
        type: Sequelize.INTEGER
    },
    posicion_y: {
        type: Sequelize.INTEGER
    },
    nro_piso: {
        type: Sequelize.INTEGER
    },
    cap_comensales: {
        type: Sequelize.INTEGER
    },

}, {
    freezeTableName: true,
    timestamps: false
});

//Mesa.hasMany(Reserva, { foreignKey: 'mesa', as: 'MesaId', sourceKey: 'id' });
//Reserva.belongsTo(Mesa, { foreignKey: 'mesa',  sourceKey: 'id' } );

Mesa.hasMany(Reserva, { foreignKey: 'mesa', targetKey: 'id'});
Reserva.belongsTo(Mesa, { foreignKey: 'mesa', as:'Mesa', targetKey: 'id'});

export default Mesa;