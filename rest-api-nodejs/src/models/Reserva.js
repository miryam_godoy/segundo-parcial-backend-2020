import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

//import Cliente from './Cliente';
//import Mesa from './Mesa';
//import Restaurante from './Restaurante';

const Reserva = sequelize.define('reservas', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    restaurante: {
        type: Sequelize.INTEGER
    },
    mesa: {
        type: Sequelize.INTEGER
    },
    fecha: {
        type: Sequelize.DATE
    },
    hora_inicio: {
        type: Sequelize.TEXT
    },
    hora_fin: {
        type: Sequelize.TEXT
    },
    cliente: {
        type: Sequelize.INTEGER

    },
    cantidad: {
        type: Sequelize.INTEGER
    }
}, {
    freezeTableName: true,
    timestamps: false
});
//import Cliente from './Cliente';


export default Reserva;