import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

import Mesa from './Mesa';
import Reserva from './Reserva';

const Restaurante = sequelize.define('restaurante', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    nombre: {
        type: Sequelize.TEXT
    },
    direccion: {
        type: Sequelize.TEXT
    }
}, {
    freezeTableName: true,
    timestamps: false
});

Restaurante.hasMany(Mesa, { foreignKey: 'restauranteid',  sourceKey: 'id' });
Mesa.belongsTo(Restaurante, { foreignKey: 'restauranteid',  sourceKey: 'id' } );

Restaurante.hasMany(Reserva, { foreignKey: 'restaurante', targetKey: 'id' });
Reserva.belongsTo(Restaurante, { foreignKey: 'restaurante', as: 'Restaurante', targetKey: 'id' });

export default Restaurante;