import { Router } from 'express';
const router = Router();

import { getClientes, getOneCliente, createCliente, updateCliente, deleteCliente } from '../controllers/cliente.controller';

// /api/clientes/
router.post('/', createCliente);
router.get('/', getClientes);

// /api/projects/projectID
router.get('/:cedula', getOneCliente);

router.delete('/:id', deleteCliente);

router.put('/:id', updateCliente)

export default router;