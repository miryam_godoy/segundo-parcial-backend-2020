import { Router } from 'express';
const router = Router();

import { getMesa, getOneMesa, createMesa, updateMesa, deleteMesa, getMesaDisponibles } from '../controllers/mesa.controller';

// /api/mesas/
router.post('/', createMesa);
router.get('/', getMesa);

// /api/mesas/mesasDisponibles
router.post('/mesasDisponibles', getMesaDisponibles);


// /api/projects/projectID
router.get('/:id', getOneMesa);

router.delete('/:id', deleteMesa);

router.put('/:id', updateMesa);


export default router;