import { Router } from 'express';
const router = Router();

import { getReservas, createReserva, getReservaPorFecha, getReservaRestaurante, getReservaCliente, deleteReserva } from '../controllers/reserva.controller';

// /api/reservas/
router.post('/addReserva', createReserva);
router.get('/', getReservas);

// /api/reservas/porFecha?fecha={fecha}
router.get('/porFecha', getReservaPorFecha);

// /api/reservas/porRestaurante?restaurante={idRestaurante}
router.get('/porRestaurante', getReservaRestaurante);

// /api/reservas/porCliente?cliente={idCliente}
router.get('/porCliente', getReservaCliente);

// /api/reservas/mesasDisponibles/
//router.get('/mesasDisponibles', getMesasDisponibles);
// /api/restaurantes/restauranteID
//router.get('/:id', getOneRestaurante);

router.delete('/:id', deleteReserva);

//router.put('/:id', updateRestaurante)

export default router;