import { Router } from 'express';
const router = Router();

import { getRestaurante, createRestaurante, getOneRestaurante, updateRestaurante, deleteRestaurante } from '../controllers/restaurante.controller';

// /api/restaurantes/
router.get('/', getRestaurante);
router.post('/', createRestaurante);

// /api/restaurantes/restauranteID
router.get('/:id', getOneRestaurante);

router.delete('/:id', deleteRestaurante);

router.put('/:id', updateRestaurante)

export default router;
